## How to install

- `git clone url`
- `cp .env.example .env`
- `composer update`
- `npm install`
- `npm run build` (**Build tailwind resource if breeze tailwind is used**)
- `php artisan key:generate`
- `php artisan serve`


### More for running the project (Without getting errors)
```sh
php artisan storage:link
mkdir -p public/storage/categories
mkdir -p public/storage/products
```

### For database
- For first use

    `php artisan migrate --seed`

- For fresh use (Database already created)

    `php artisan migrate:fresh --seed`
