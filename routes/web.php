<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

// http verb: get, post, put/patch, delete

Route::get('/', [WelcomeController::class, 'welcome']);
Route::get('/caterories/{category}/products', [WelcomeController::class, 'productList'])->name('frontend.products');

Route::get('/products/{product}', [WelcomeController::class, 'productDetails'])->name('frontend.products.show');

require __DIR__ . '/auth.php';
Route::middleware('auth')->prefix('dashboard')->group(function(){

    Route::get('/', function () {
        return view('home');
    });

    Route::get('/categories-trash', [CategoryController::class, 'trash'])->name('categories.trash');
    Route::get('/categories/{id}/restore', [CategoryController::class, 'restore'])->name('categories.restore');
    Route::delete('/categories/{id}/delete', [CategoryController::class, 'delete'])->name('categories.delete');
    Route::get('/categories/pdf', [CategoryController::class, 'downloadPdf'])->name('categories.pdf');
    
    Route::resource('categories', CategoryController::class);

    Route::get('/colors-trash', [ColorController::class, 'trash'])->name('colors.trash');
    Route::get('/colors/{id}/restore', [ColorController::class, 'restore'])->name('colors.restore');
    Route::delete('/colors/{id}/delete', [ColorController::class, 'delete'])->name('colors.delete');
    Route::get('/colors/pdf', [ColorController::class, 'downloadPdf'])->name('colors.pdf');
    Route::resource('colors', ColorController::class);

    
    Route::get('/products-trash', [ProductController::class, 'trash'])->name('products.trash');
    Route::get('/products/{id}/restore', [ProductController::class, 'restore'])->name('products.restore');
    Route::delete('/products/{id}/delete', [ProductController::class, 'delete'])->name('products.delete');
    Route::get('/products/pdf', [ProductController::class, 'downloadPdf'])->name('products.pdf');
    Route::resource('products', ProductController::class);
    
    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('users.index');
        Route::get('/{user}', [UserController::class, 'show'])->name('users.show');
        Route::get('/{user}/change-role', [UserController::class, 'changeRole'])->name('users.change_role');
        Route::patch('/{user}/change-role', [UserController::class, 'updateRole'])->name('users.update_role');
    });

    Route::post('products/{product}/cart', [CartController::class, 'store'])->name('products.cart.store');
});

Route::redirect('/here', '/users');

Route::fallback(function () {
    echo 'Apnar url thik nai';
});
