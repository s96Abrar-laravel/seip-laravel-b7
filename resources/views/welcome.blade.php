<x-frontend.master>

    {{-- <x-frontend.carousel /> --}}

    <div class="container marketing">
        <br><br><br><br>
        <!-- Three columns of text below the carousel -->
        <div class="row">

            @foreach($products as $product)
            <div class="col-lg-4 mb-2">
                <div class="card">
                    <div class="card-header">
                        <img height="250" src="{{ asset('storage/products/'.$product->image) }}" alt="{{ $product->title }}" />
                    </div>
                    <div class="card-body">
                        <h3>{{ Str::limit($product->title, 40) }}</h3>
                        <p>{{ Str::limit($product->description, 50) }}</p>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-info btn-sm" href="{{ route('frontend.products.show', $product->id) }}">View details &raquo;</a>
                        <a class="btn btn-primary btn-sm" href="#">Add to Card</a>
                    </div>
                </div>
            </div><!-- /.col-lg-4 -->
            @endforeach
        </div><!-- /.row -->

        {{ $products->links() }}
        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->
</x-frontend.master>