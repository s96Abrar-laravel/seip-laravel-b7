<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // DB::table('categories')->insert([
        //     'title' => 'Category 1',
        //     'is_active' => true
        // ]);

        Category::create([
            'title' => 'Kids',
            'is_active' => true
        ]);

        Category::create([
            'title' => 'Men',
            'is_active' => true
        ]);

        Category::create([
            'title' => 'Women',
            'is_active' => true
        ]);

        Category::create([
            'title' => 'Lifestyle',
            'is_active' => false
        ]);
    }
}
